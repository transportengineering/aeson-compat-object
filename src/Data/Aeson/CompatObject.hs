{-# LANGUAGE CPP #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}

module Data.Aeson.CompatObject
  ( pattern Object,
    objectToHashMapText,
    objectFromHashMapText,
  )
where

import Data.Aeson (Value (Array, Bool, Null, Number, String))
import qualified Data.Aeson as A
import Data.HashMap.Strict (HashMap)
import Data.Text (Text)

#if MIN_VERSION_aeson(2,0,0)
import qualified Data.Aeson.KeyMap as KM
import qualified Data.HashMap.Strict as H

objectToHashMapText :: A.Object -> HashMap Text A.Value
objectToHashMapText = KM.toHashMapText

objectFromHashMapText :: HashMap Text A.Value -> A.Object
objectFromHashMapText = KM.fromHashMapText

#else
objectToHashMapText :: A.Object -> HashMap Text A.Value
objectToHashMapText = id

objectFromHashMapText :: HashMap Text A.Value -> A.Object
objectFromHashMapText = id
#endif

pattern Object :: HashMap Text A.Value -> A.Value
pattern Object a <-
  A.Object (objectToHashMapText -> a)
  where
    Object a = A.Object (objectFromHashMapText a)

{-# COMPLETE Object, Array, String, Number, Bool, Null #-}
